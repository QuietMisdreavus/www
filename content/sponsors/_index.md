+++
title = "Sponsors"
template = "sponsors/index.html"
[extra.sponsors]
silver = [
  ["SALT Lending", "logos/salt.svg", "https://www.saltlending.com"],
]
prize = [
  ["Manning Publications", "logos/manning.svg", "https://www.manning.com"],
  ["No Starch Press", "logos/no-starch.svg", "https://nostarch.com"],
]
+++
